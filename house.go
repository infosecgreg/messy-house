package main

import(
	"gitlab.com/infosecgreg/messy-house/server"
)

func main() {
	var s *server.Server = new(server.Server)
	s.Run()
}
