package server

import(
	"fmt"
	"net/http"
	"sync"
)

type Server struct {
	waitGroup sync.WaitGroup
}

func resp(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "super hacker command to pwn systems!")
}

func (s *Server) listen() {
	http.HandleFunc("/", resp)
	http.ListenAndServe(":8888", nil)
	s.waitGroup.Done()
}

func (s *Server) Run() {
	s.waitGroup.Add(1)
	go s.listen()
	s.waitGroup.Wait()
}
